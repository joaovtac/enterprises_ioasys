import 'package:enterprises_ioasys/app/app_module.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/usecases/build_listen_stocks_websocket.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/usecases/connect_stocks_websocket.dart';
import 'package:enterprises_ioasys/app/modules/stocks/stock_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

main() async {

  initModules([
    AppModule(),
    StockModule(),
  ]);

  test("should get usecase ConnectStocksWebSocket", () {
    final usecase = Modular.get<ConnectStocksWebSocket>();
    expect(usecase, isA<ConnectStocksWebSocketImpl>());
  });

  test("should get usecase BuildListenStocksWebSocket", () {
    final usecase = Modular.get<BuildListenStocksWebSocket>();
    expect(usecase, isA<BuildListenStocksWebSocketImpl>());
  });
}
