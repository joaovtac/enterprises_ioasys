
import 'package:enterprises_ioasys/app/core/utils/utils.dart';
import 'package:enterprises_ioasys/app/core/wrappers/web_socket_wrapper.dart';
import 'package:enterprises_ioasys/app/modules/stocks/external/datasources/stocks_websocket_datasource_Impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class WebSocketChannelWrapperMock extends Mock implements WebSocketChannelWrapper {}

class WebSocketChannelMock extends Mock implements WebSocketChannel{}

main() {
  final webSocketChannelWrapperMock = WebSocketChannelWrapperMock();
  final webSocketChannelMock = WebSocketChannelMock();

  final datasource = StocksWebSocketDatasourceImpl(webSocketChannelWrapperMock);
  final uri = Uri.parse(AppConfig.URL_WEB_SOCKET);

  group("getStreamStocks", () {
    test('should get Stream Stocks', () async {
      when(webSocketChannelWrapperMock.connect(uri)).thenAnswer((_) => webSocketChannelMock);
      final result = datasource.getStockWebSocket();

      expect(result, webSocketChannelMock);
    });
  });
}
