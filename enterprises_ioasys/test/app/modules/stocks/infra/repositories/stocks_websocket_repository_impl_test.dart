
import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/entities/stock.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/stocks/infra/datasources/stocks_websocket_datasource.dart';
import 'package:enterprises_ioasys/app/modules/stocks/infra/repositories/stocks_websocket_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class StocksWebSocketDatasourceMock extends Mock implements StocksWebSocketDatasource {}

class WebSocketChannelStock extends Mock implements WebSocketChannel {}

main() {
  final datasource = StocksWebSocketDatasourceMock();
  final repository = StocksWebSocketRepositoryImpl(datasource);

  final dataStocks = '{"stocks": [{"name": "name", "tag": "tag", "price": 0.0}]}';
  final dataStocksBroken= '{"stocks": [{"name": "name", "tag": "tag", "price": 0.0}}';
  final dataStocksEmpty = '{"stocks": []}';
  final webSocketChannelStock = WebSocketChannelStock();

  group("getStockWebSocket", () {
    test('should get Stream Stocks', () async {
      when(datasource.getStockWebSocket()).thenAnswer((_) => webSocketChannelStock);
      var result = repository.getStreamStocks();
      expect(result, isA<Right<dynamic, WebSocketChannel>>());
    });

    test('should call ErrorStocksWebSocket Failed To Recover Websocket', () async {
      when(datasource.getStockWebSocket()).thenThrow(ErrorStocksWebSocket());
      var result = repository.getStreamStocks();
      expect(result.leftMap((l) => l is ErrorStocksWebSocket), Left(true));
      expect(result.fold((l) => l.message, id), equals(Messages.FAILED_TO_RECOVER_WEBSOCKET_STOCKS));
    });
  });

  group("buildStocks", () {

    test('should return List Stocks', () async {
      var result = repository.buildStocks(dataStocks);
      expect(result | null, isA<List<Stock>>());

      List<Stock> listStocks = result.getOrElse(() => null);

      expect(listStocks.length, equals(1));

      expect(listStocks.first.name, equals("name"));
      expect(listStocks.first.tag, equals("tag"));
      expect(listStocks.first.price, equals(0.0));
    });

    test('should return ErrorStocksWebSocket None Stocks Listed ', () async {
      var result = repository.buildStocks(dataStocksEmpty);
      expect(result.fold(id, id), isA<ErrorStocksWebSocket>());
      expect(result.fold((l) => l.message, id), equals(Messages.NONE_STOCKS_LISTED));
    });

    test('should return ErrorStocksWebSocket Data Invalid', () async {
      var result =  repository.buildStocks(dataStocksBroken);
      expect(result.fold(id, id), isA<ErrorStocksWebSocket>());
      expect(result.fold((l) => l.message, id), equals(Messages.DATA_INVALID_WEBSOCKET_STOCKS));
    });
  });
}
