import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/services/connectivity_service.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/repositories/stocks_websocket_repository.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/usecases/connect_stocks_websocket.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mockito/mockito.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class StocksWebSocketRepositoryMock extends Mock implements StocksWebSocketRepository {}

class ConnectivityServiceMock extends Mock implements ConnectivityService {}

class WebSocketChannelStock extends Mock implements WebSocketChannel {}

main() {
  final repository = StocksWebSocketRepositoryMock();
  final service = ConnectivityServiceMock();
  final usecase = ConnectStocksWebSocketImpl(repository, service);

  setUpAll(() {
    when(service.isOnline()).thenAnswer((_) async => Right(unit));
  });


  test('should consume repository StocksWebSocketRepository', () async {
    var webSocketChannelStock = WebSocketChannelStock();
    when(repository.getStreamStocks()).thenAnswer((_) => Right(webSocketChannelStock));
    var result = await usecase();
    expect(result, Right(webSocketChannelStock));
  });

  test('should return error when offline', () async {
    when(service.isOnline()).thenAnswer((_) async => Left(ErrorStocksWebSocket()));
    var result = await usecase();
    expect(result.leftMap((l) => l is ErrorStocksWebSocket), Left(true));
  });
}
