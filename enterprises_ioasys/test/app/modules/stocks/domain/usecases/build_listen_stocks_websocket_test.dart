
import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/services/connectivity_service.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/entities/stock.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/repositories/stocks_websocket_repository.dart';
import 'package:enterprises_ioasys/app/modules/stocks/domain/usecases/build_listen_stocks_websocket.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class StocksWebSocketRepositoryMock extends Mock implements StocksWebSocketRepository {}
class ConnectivityServiceMock extends Mock implements ConnectivityService {}

main() {
  final repository = StocksWebSocketRepositoryMock();
  final service = ConnectivityServiceMock();
  final usecase = BuildListenStocksWebSocketImpl(repository, service);

  setUpAll(() {
    when(service.isOnline()).thenAnswer((_) async => Right(unit));
  });

  final dataStocks = "{'stocks': [{'name': 'name', 'tag': 'tag', 'price': 0.0}]}";

  test('should return Data Invalid WebSocket to Null', () async {
    var result = await usecase(null);
    expect(result.fold(id, id), isA<ErrorStocksWebSocket>());
    expect(result.fold((l) => l.message, id), equals(Messages.DATA_INVALID_WEBSOCKET_STOCKS));
  });

  test('should return Data Invalid WebSocket to String Empty ', () async {
    var result = await usecase("");
    expect(result.fold(id, id), isA<ErrorStocksWebSocket>());
    expect(result.fold((l) => l.message, id), equals(Messages.DATA_INVALID_WEBSOCKET_STOCKS));
  });

  test('should return list with Stocks', () async {
    when(repository.buildStocks(dataStocks))
        .thenAnswer((_) => right(<Stock>[Stock()]));

    var result = await usecase(dataStocks);
    expect(result | null, isA<List<Stock>>());
  });

  test('should return error when offline', () async {
    when(service.isOnline()).thenAnswer((_) async => Left(ConnectionError()));

    var result = await usecase(dataStocks);
    expect(result.leftMap((l) => l is ConnectionError), Left(true));
  });
}


