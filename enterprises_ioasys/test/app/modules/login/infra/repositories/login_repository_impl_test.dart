import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/entities/logged_user_info.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/login/infra/datasources/login_datasource.dart';
import 'package:enterprises_ioasys/app/modules/login/infra/models/logged_user_model.dart';
import 'package:enterprises_ioasys/app/modules/login/infra/repositories/login_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class LoginDataSourceMock extends Mock implements LoginDataSource {}

main() {
  final datasource = LoginDataSourceMock();
  final userReturn = LoggedUserModel(
    uid: "uid",
    client: "cliente",
    accessToken: "access-token"
  );
  final repository = LoginRepositoryImpl(datasource);

  final responseInvalidCredentials = Response(data: {"errors" : ["Invalid login credentials. Please try again."]});

  group("loginEmail", () {
    test('should get UserModel', () async {
      when(datasource.loginEmail()).thenAnswer((_) async => userReturn);
      var result = await repository.loginEmail();
      expect(result, isA<Right<dynamic, LoggedUserInfo>>());
    });

    test('should call ErrorInvalidCredentials Invalid Credentials', () async {
      when(datasource.loginEmail()).thenThrow(DioError(response: responseInvalidCredentials));
      var result = await repository.loginEmail();
      expect(result.leftMap((l) => l is ErrorInvalidCredentials), Left(true));
      expect(result.fold((l) => l.message, id), equals(Messages.INVALID_CREDENTIALS));
    });

    test('should call ErrorLoginWithEmail', () async {
      when(datasource.loginEmail()).thenThrow(DioError());
      var result = await repository.loginEmail();
      expect(result.leftMap((l) => l is ErrorLoginWithEmail), Left(true));
    });

    test('should call ErrorLoginWithEmail', () async {
      when(datasource.loginEmail()).thenThrow(ErrorLoginWithEmail());
      var result = await repository.loginEmail();
      expect(result.leftMap((l) => l is ErrorLoginWithEmail), Left(true));
    });
  });
}
