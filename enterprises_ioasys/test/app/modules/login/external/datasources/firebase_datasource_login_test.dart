import 'package:dio/dio.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/entities/logged_user.dart';
import 'package:enterprises_ioasys/app/modules/login/extenal/datasources/login_datasource_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class DioMock extends Mock implements Dio {}

class ResponseMock extends Mock implements Response {}


main() {
  final dio = DioMock();
  final datasource = LoginDataSourceImpl(dio);

  final response = ResponseMock();

  var headers = Headers();
  headers.add("uid","uid");
  headers.add("client", "client");
  headers.add("access-token", "access-token",);


  test('should return Logged User loginEmail', () async {
    when(dio.post(any, data: dataCredential)).thenAnswer((_) async => response);
    when(response.headers).thenReturn(headers);
    var result = await datasource.loginEmail(
      email: "testeapple@ioasys.com.br",
      password: "12341234",
    );

    expect(result, isA<LoggedUser>());
    expect(result.uid, equals("uid"));
    expect(result.client, equals("client"));
    expect(result.accessToken, equals("access-token"));
  });
}

final dataCredential = {
  "email": "testeapple@ioasys.com.br",
  "password": "12341234",
};
