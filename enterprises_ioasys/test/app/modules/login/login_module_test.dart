import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:enterprises_ioasys/app/app_module.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/usecases/login_with_email.dart';
import 'package:enterprises_ioasys/app/modules/login/login_module.dart';

main() async {

  initModules([
    AppModule(),
    LoginModule(),
  ]);

  test("should get usecase LoginWithEmail", () {
    final usecase = Modular.get<LoginWithEmail>();
    expect(usecase, isA<LoginWithEmailImpl>());
  });
}
