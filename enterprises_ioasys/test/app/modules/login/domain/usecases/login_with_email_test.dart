import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/modules/login/infra/models/logged_user_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/services/connectivity_service.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/entities/login_credential.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/repositories/login_repository.dart';
import 'package:enterprises_ioasys/app/modules/login/domain/usecases/login_with_email.dart';
import 'package:mockito/mockito.dart';

class LoginRepositoryMock extends Mock implements LoginRepository {}

class ConnectivityServiceMock extends Mock implements ConnectivityService {}

main() {
  final repository = LoginRepositoryMock();
  final service = ConnectivityServiceMock();
  final usecase = LoginWithEmailImpl(repository, service);

  setUpAll(() {
    when(service.isOnline()).thenAnswer((_) async => Right(unit));
  });

  test('should verify if credentials have been entered', () async {
    var result = await usecase(
        LoginCredential.withEmailAndPassword(email: "", password: ""));
    expect(result.leftMap((l) => l is ErrorLoginWithEmail), Left(true));
    expect(result.fold((l) => l.message, id), Messages.INVALID_INSERT_CREDENTIALS);
  });

  test('should verify if email is not valid', () async {
    var result = await usecase(
        LoginCredential.withEmailAndPassword(email: "", password: "password"));
    expect(result.leftMap((l) => l is ErrorLoginWithEmail), Left(true));
    expect(result.fold((l) => l.message, id), Messages.INVALID_FORMAT_EMAIL);
  });

  test('should verify if password is not valid', () async {
    var result = await usecase(LoginCredential.withEmailAndPassword(
        email: "testeapple@ioasys.com.br", password: ""));
    expect(result.leftMap((l) => l is ErrorInvalidCredentials), Left(true));
    expect(result.fold((l) => l.message, id), Messages.INVALID_CREDENTIALS);
  });

  test('should consume repository loginEmail', () async {
    var user = LoggedUserModel(uid: "uid", client: "client", accessToken: "access-token");
    when(repository.loginEmail(
            email: anyNamed('email'), password: anyNamed('password')))
        .thenAnswer((_) async => Right(user));
    var result = await usecase(LoginCredential.withEmailAndPassword(
        email: "testeapple@ioasys.com.br", password: "password"));

    expect(result, Right(user));
  });

  test('should return error when offline', () async {
    when(service.isOnline()).thenAnswer((_) async => Left(ConnectionError()));

    var result = await usecase(LoginCredential.withEmailAndPassword(
        email: "testeapple@ioasys.com.br", password: "password"));
    expect(result.leftMap((l) => l is ConnectionError), Left(true));
  });
}
