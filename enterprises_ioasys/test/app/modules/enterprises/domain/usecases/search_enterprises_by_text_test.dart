
import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/services/connectivity_service.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/domain/entities/enterprise.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/domain/repositories/enterprises_repository.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/domain/usecases/search_enterprises_by_text.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class EnterprisesRepositoryMock extends Mock implements EnterprisesRepository {}
class ConnectivityServiceMock extends Mock implements ConnectivityService {}

main() {
  final repository = EnterprisesRepositoryMock();
  final service = ConnectivityServiceMock();
  final usecase = SearchEnterprisesByTextImpl(repository, service);

  setUpAll(() {
    when(service.isOnline()).thenAnswer((_) async => Right(unit));
  });

  test('should return list with enterprises', () async {
    when(repository.getEnterprises("Enterprise"))
        .thenAnswer((_) async => right(<Enterprise>[Enterprise()]));

    var result = await usecase("Enterprise");
    expect(result | null, isA<List<Enterprise>>());
  });

  test('should return ErrorSearch ', () async {
    when(repository.getEnterprises("Enterprise"))
        .thenAnswer((_) async => Left(ErrorSearch(Messages.FAILED_TO_SEARCH)));

   var result = await usecase("Enterprise");
    expect(result.fold(id, id), isA<ErrorSearch>());
    expect(result.fold((l) => l.message, id), equals(Messages.FAILED_TO_SEARCH));
  });

  test('should return InvalidSearchText', () async {
        var result = await usecase(null);
        expect(result.fold(id, id), isA<InvalidSearchText>());
  });

  test('should return EmptyList', () async {
    when(repository.getEnterprises("Enterprise")).thenAnswer((_) async => right(<Enterprise>[]));

    var result = await usecase("Enterprise");
    expect(result.fold(id, id), isA<EmptyList>());
  });

  test('should return error when offline', () async {
    when(service.isOnline()).thenAnswer((_) async => Left(ConnectionError()));

    var result = await usecase("Enterprise");
    expect(result.leftMap((l) => l is ConnectionError), Left(true));
  });
}
