
import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/infra/datasources/enterprises_datasource.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/infra/models/enterprise_model.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/infra/repositories/enterprises_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class EnterprisesDatasourceMock extends Mock implements EnterprisesDatasource {}

main() {
  final datasource = EnterprisesDatasourceMock();
  final repository = EnterprisesRepositoryImpl(datasource);

  test('should return list EnterpriseModel', () async {
    when(datasource.getListEnterprises("Enterprise")).thenAnswer((_) async => <EnterpriseModel>[EnterpriseModel()]);

    var result = await repository.getEnterprises("Enterprise");
    expect(result | null, isA<List<EnterpriseModel>>());
  });

  test('should return ErrorSearch ', () async {
    when(datasource.getListEnterprises("Enterprise")).thenThrow(ErrorSearch(Messages.FAILED_TO_SEARCH));

    var result = await repository.getEnterprises("Enterprise");
    expect(result.fold(id, id), isA<ErrorSearch>());
    expect(result.fold((l) => l.message, id), equals(Messages.FAILED_TO_SEARCH));
  });
  test('should return DatasourceResultNull', () async {
    when(datasource.getListEnterprises("Enterprise")).thenAnswer((_) async => null);

    var result = await repository.getEnterprises("Enterprise");
    expect(result.fold(id, id), isA<DatasourceResultNull>());
    expect(result.fold((l) => l.message, id), equals(Messages.FAILED_TO_SEARCH));
  });
}
