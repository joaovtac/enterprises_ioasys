import 'package:enterprises_ioasys/app/app_module.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/domain/usecases/search_enterprises_by_text.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/enterprises_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

main() async {

  initModules([
    AppModule(),
    EnterprisesModule(),
  ]);

  test("should get usecase SearchEnterprisesByTextImpl", () {
    final usecase = Modular.get<SearchEnterprisesByText>();
    expect(usecase, isA<SearchEnterprisesByTextImpl>());
  });
}
