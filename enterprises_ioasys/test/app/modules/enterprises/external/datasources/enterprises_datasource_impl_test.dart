import 'package:dio/dio.dart';
import 'package:enterprises_ioasys/app/core/stores/auth_store.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/external/datasources/enterprises_datasource_impl.dart';
import 'package:enterprises_ioasys/app/modules/enterprises/infra/models/enterprise_model.dart';
import 'package:enterprises_ioasys/app/modules/login/infra/models/logged_user_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class DioMock extends Mock implements Dio {}

class BaseOptionsMock extends Mock implements BaseOptions {}

class AuthStoreMock extends Mock implements AuthStore {}

main() {
  final dio = DioMock();
  final options = BaseOptionsMock();
  final authStore = AuthStoreMock();

  final Map<String, dynamic> headers = {};

  var datasource = EnterprisesDatasourceImpl(dio, authStore);

  setUpAll(() {
    when(authStore.user).thenReturn(LoggedUserModel(
      uid: "udi",
      client: "client",
      accessToken: "acess-token",
    ));
  });

  test('should return list EnterpriseModels', () async {
    when(dio.options).thenReturn(options);
    when(options.headers).thenReturn(headers);
    when(dio.get(any)).thenAnswer((_) async => Response(data: jsonResponse, statusCode: 200));

    var result = await datasource.getListEnterprises("Enterprise");
    expect(result, isA<List<EnterpriseModel>>());
    expect(result.length, equals(2));

    expect(result.first.id, equals(10));
    expect(result.first.enterprise_name, equals("Nitechain & Nightset"));
    expect(result.first.description, equals("description"));
    expect(result.first.city, equals("London"));
    expect(result.first.country, equals("UK"));
    expect(result.first.share_price, equals(5000.0));
  });


  test('should return Exception', () async {
    when(dio.options).thenReturn(options);
    when(options.headers).thenReturn(headers);
    when(dio.get(any)).thenAnswer((_) async => Response(data: {"errors: errors"}, statusCode: 401));

    expect(datasource.getListEnterprises("Enterprise"), throwsA(isA<Exception>()));
  });
}

var jsonResponse = {
  "enterprises": [
    {
      "id": 10,
      "email_enterprise": null,
      "facebook": null,
      "twitter": null,
      "linkedin": null,
      "phone": null,
      "own_enterprise": false,
      "enterprise_name": "Nitechain & Nightset",
      "photo": "/uploads/enterprise/photo/10/240.jpeg",
      "description": "description",
      "city": "London",
      "country": "UK",
      "value": 0,
      "share_price": 5000.0,
      "enterprise_type": {"id": 13, "enterprise_type_name": "Social"}
    },
    {
      "id": 14,
      "email_enterprise": null,
      "facebook": null,
      "twitter": null,
      "linkedin": null,
      "phone": null,
      "own_enterprise": false,
      "enterprise_name": "Halo Active Technologies ",
      "photo": "/uploads/enterprise/photo/14/240.jpeg",
      "description": "description",
      "city": "Hove",
      "country": "UK",
      "value": 0,
      "share_price": 5000.0,
      "enterprise_type": {"id": 24, "enterprise_type_name": "Transport"}
    },
  ]
};
