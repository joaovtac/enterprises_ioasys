import 'package:dartz/dartz.dart';
import 'package:enterprises_ioasys/app/core/connectivity/domain/errors/errors.dart';
import 'package:enterprises_ioasys/app/core/connectivity/infra/drivers/connectivity_driver.dart';
import 'package:enterprises_ioasys/app/core/connectivity/infra/services/connectivity_service_impl.dart';
import 'package:enterprises_ioasys/app/core/errors/messages.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class ConnectivityDriverMock extends Mock implements ConnectivityDriver {}

main() {
  final driver = ConnectivityDriverMock();
  final service = ConnectivityServiceImpl(driver);

  group("ConnectivityServiceImpl", () {
    test('should return bool', () async {
      when(driver.isOnline).thenAnswer((_) async => true);
      var result = await service.isOnline();
      expect(result, isA<Right<dynamic, Unit>>());
    });
    test('should return User is OffLine', () async {
      when(driver.isOnline).thenAnswer((_) async => false);
      var result = await service.isOnline();
      expect(result.leftMap((l) => l is ConnectionError), Left(true));
      expect(result.fold((l) => l.message, id), equals(Messages.OFFLINE_CONNECTION));
    });
    test('should call ErrorLoginEmail', () async {
      when(service.isOnline()).thenThrow(ConnectionError());
      var result = await service.isOnline();
      expect(result.leftMap((l) => l is ConnectionError), Left(true));
    });
  });
}
