import 'package:flutter/material.dart';

///This class defines aspects like colors used in every app
class ThemeApp{
  ThemeApp._();
  ///Main color used in every app
  static const Color empresasIoasysColor = Color(0xFFE01E69);
  static const Color greyColor = Color(0xFFF5F5F5);
  static const Color secondGreyColor = Color(0xFF66666);
  static const Color circularProgressColor = Color(0xFFFBDBE7);

  static const Color labelColor = Color(0xFF666666);
  static const Color redColor = Color(0xFFE00000);

  ///Main Materialcolor used in every application
  static const MaterialColor empresasIoasysMaterialColor =  MaterialColor(
    0xFFC2175B,
     <int, Color>{
      50:  Color(0xFFE01E69),
      100: Color(0xFFE01E69),
      200: Color(0xFFE01E69),
      300: Color(0xFFE01E69),
      400: Color(0xFFE01E69),
      500: Color(0xFFE01E69),
      600: Color(0xFFE01E69),
      700: Color(0xFFE01E69),
      800: Color(0xFFE01E69),
      900: Color(0xFFE01E69),
    },
  );
}