import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_widget.dart';
import 'core/connectivity/external/drivers/flutter_connectivity_driver_impl.dart';
import 'core/connectivity/infra/services/connectivity_service_impl.dart';
import 'core/pages/spash_screen_page.dart';
import 'core/stores/auth_store.dart';
import 'modules/enterprises/enterprises_module.dart';
import 'modules/login/login_module.dart';
import 'modules/stocks/stock_module.dart';
import 'modules/utils/loading_dialog.dart';

///This is main module define to every app
class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        $AuthStore,
        $LoadingDialogImpl,
        $ConnectivityServiceImpl,
        $FlutterConnectivityDriver,
        Bind((i) => Dio()),
        Bind((i) => Connectivity()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter("/", child: (_, __) => SplashScreenPage()),
        ModularRouter("/login", module: LoginModule()),
        ModularRouter("/enterprises", module: EnterprisesModule()),
        ModularRouter("/stocks", module: StockModule()),
  ];

  @override
  Widget get bootstrap => AppWidget();
}
