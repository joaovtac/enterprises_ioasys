import 'package:flutter/material.dart';

Size sizedScreen(BuildContext context) {
  return MediaQuery.of(context).size;
}

class AppConfig{
  static String get ENV => "https://empresas.ioasys.com.br";
  static String get API_VERSION => "/v1";
  static String get URL_WEB_SOCKET => "wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self";
}