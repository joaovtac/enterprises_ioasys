import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../stores/auth_store.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

splashScreenPage() {
  Future.delayed(Duration(seconds: 3)).whenComplete(() => {
  Modular.get<AuthStore>().isLogged
  ? Modular.to.pushNamedAndRemoveUntil("/enterprises", (_) => false)
      : Modular.to.pushNamedAndRemoveUntil("/login", (_) => false)
  });
}

class _SplashScreenPageState extends State<SplashScreenPage> with TickerProviderStateMixin {
  AnimationController animation;
  Animation<double> _fadeInFadeOut;

  @override
  void initState() {
    super.initState();
    splashScreenPage();
    animationController();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Image.asset(
            "assets/app/core/pages/background.png",
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
          ),
          FadeTransition(
            opacity: _fadeInFadeOut,
            child: Center(
              child: Image.asset(
                "assets/app/core/pages/ioasys_logo.png",
                width: 185,
                height: 45,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void animationController() {
    animation = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    _fadeInFadeOut = Tween<double>(begin: 0.0, end: 1).animate(animation);

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animation.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animation.forward();
      }
    });
    animation.forward();
  }

  @override
  void dispose() {
    animation.dispose();
    super.dispose();
  }
}
