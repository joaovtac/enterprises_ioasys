import '../../../errors/errors.dart';

class ConnectionError extends Failure {
  final String message;
  ConnectionError({this.message});
}