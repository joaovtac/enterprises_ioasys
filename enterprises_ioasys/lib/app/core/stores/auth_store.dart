import 'package:flutter_modular/flutter_modular.dart';
import '../../modules/login/domain/entities/logged_user_info.dart';
import 'package:mobx/mobx.dart';

part 'auth_store.g.dart';

@Injectable()
class AuthStore = _AuthStoreBase with _$AuthStore;

abstract class _AuthStoreBase with Store {

  @observable
  LoggedUserInfo user;

  @computed
  bool get isLogged => user != null;

  @action
  void setUser(LoggedUserInfo value) => user = value;

  void signOut() {
      setUser(null);
      Modular.to.pushNamedAndRemoveUntil("/login", (_) => false);
  }
}
