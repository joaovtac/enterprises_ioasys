import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

part 'web_socket_wrapper.g.dart';

@Injectable()
class WebSocketChannelWrapper = _WebSocketChannelWrapperBase with _$WebSocketChannelWrapper;

abstract class _WebSocketChannelWrapperBase with Store {

  WebSocketChannel connect(Uri uri) {
    return WebSocketChannel.connect(uri);
  }
}