// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'web_socket_wrapper.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $WebSocketChannelWrapper = BindInject(
  (i) => WebSocketChannelWrapper(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$WebSocketChannelWrapper on _WebSocketChannelWrapperBase, Store {
  @override
  String toString() {
    return '''

    ''';
  }
}
