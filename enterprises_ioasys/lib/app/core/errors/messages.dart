class Messages {

  //MESSAGES TO LOGIN USER
  static String get INVALID_INSERT_CREDENTIALS => "Insira suas crendencias";
  static String get INVALID_FORMAT_EMAIL => "Formato de email inválido";
  static String get INVALID_CREDENTIALS => "Credenciais incorretas";
  static String get FAILED_TO_LOGIN => "Falha no login";

  //MESSAGES TO CONNECTION USER
  static String get OFFLINE_CONNECTION => 'Você está offline';
  static String get FAILED_CONNECTION => 'Erro ao recuperar informação de conexão';

  //MESSAGES TO CONNECTION ENTERPRISES
  static String get IS_EMPTY_LIST => 'Nenhum resultado encontrado';
  static String get INVALID_TEXT_TO_SERACH => 'Texto inválido para pesquisa';
  static String get FAILED_TO_SEARCH => 'Falha ao realizar busca';

  //MESSAGES TO WEBSOCKET
  static String get FAILED_TO_RECOVER_WEBSOCKET_STOCKS => 'Falha ao realizar conexão com WebSocket de Ações';
  static String get DATA_INVALID_WEBSOCKET_STOCKS => 'A stream não possui dados legíveis!';
  static String get NONE_STOCKS_LISTED => 'Nenhuma ação foi listada pela stream!';

}