import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

import '../../app_theme.dart';

class CustomCircularProgressIndicator extends StatefulWidget {
  @override
  _CustomCircularProgressIndicatorState createState() => _CustomCircularProgressIndicatorState();
}

class _CustomCircularProgressIndicatorState extends State<CustomCircularProgressIndicator> with TickerProviderStateMixin{
  AnimationController animation;

  @override
  void initState() {
    super.initState();
    animationController();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: [
          RotationTransition(
            turns: Tween(begin: 0.0, end: 1.0).animate(animation),
            child: Center(
              child: Stack(
                children: [
                  SizedBox(
                    height: 72,
                    child: SfRadialGauge(axes: <RadialAxis>[
                      RadialAxis(
                        showLabels: false,
                        showTicks: false,
                        axisLineStyle: AxisLineStyle(
                          thickness: 0.1,
                          cornerStyle: CornerStyle.bothCurve,
                          color: ThemeApp.circularProgressColor,
                          thicknessUnit: GaugeSizeUnit.factor,
                        ),
                      )
                    ]),
                  ),
                ],
              ),
            ),
          ),
          RotationTransition(
            turns: Tween(begin: 1.0, end: 0.0).animate(animation),
            child: Center(
              child: Stack(
                children: [
                  SizedBox(
                    height: 47,
                    child: SfRadialGauge(axes: <RadialAxis>[
                      RadialAxis(
                        showLabels: false,
                        showTicks: false,
                        axisLineStyle: AxisLineStyle(
                          thickness: 0.1,
                          cornerStyle: CornerStyle.bothCurve,
                          color: ThemeApp.circularProgressColor,
                          thicknessUnit: GaugeSizeUnit.factor,
                        ),
                      )
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void animationController() {
    animation = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animation.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animation.forward();
      }
    });
    animation.forward();
  }

  @override
  void dispose() {
    animation.dispose();
    super.dispose();
  }
}