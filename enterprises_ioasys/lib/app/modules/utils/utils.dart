
import 'package:flutter_masked_text/flutter_masked_text.dart';

String realNotation(double value) {
  MoneyMaskedTextController moneyMasked = new MoneyMaskedTextController(leftSymbol: 'R\$ ', initialValue: value);
  return moneyMasked.text.toString();
}