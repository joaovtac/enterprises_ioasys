import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'domain/usecases/search_enterprises_by_text.dart';
import 'external/datasources/enterprises_datasource_impl.dart';
import 'infra/repositories/enterprises_repository_impl.dart';
import 'presentation/pages/details_enterprise_page/details_enterprise_page.dart';
import 'presentation/pages/details_enterprise_page/widgets/details_enterprise_controller.dart';
import 'presentation/pages/search_enterprises_page/search_enterprises_controller.dart';
import 'presentation/pages/search_enterprises_page/search_enterprises_page.dart';

class EnterprisesModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $SearchEnterprisesByTextImpl,
        $EnterprisesRepositoryImpl,
        $EnterprisesDatasourceImpl,
        $SearchEnterprisesController,
        $DetailsEnterpriseController,
        Bind((i) => Dio()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter("/", child: (context, args) => SearchEnterprisesPage()),
        ModularRouter("/details", child: (context, args) => DetailsEnterprisePage()),
  ];
}
