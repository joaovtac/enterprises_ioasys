// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enterprise_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EnterpriseModel _$EnterpriseModelFromJson(Map<String, dynamic> json) {
  return EnterpriseModel(
    id: json['id'] as int,
    enterprise_name: json['enterprise_name'] as String,
    photo: json['photo'] as String,
    description: json['description'] as String,
    city: json['city'] as String,
    country: json['country'] as String,
    share_price: (json['share_price'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$EnterpriseModelToJson(EnterpriseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'enterprise_name': instance.enterprise_name,
      'photo': instance.photo,
      'description': instance.description,
      'city': instance.city,
      'country': instance.country,
      'share_price': instance.share_price,
    };
