
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../modules/enterprises/domain/entities/enterprise.dart';

part 'enterprise_model.g.dart';

@JsonSerializable()
class EnterpriseModel extends Enterprise {
  EnterpriseModel({
    @required int id,
    @required String enterprise_name,
    @required String photo,
    @required String description,
    @required String city,
    @required String country,
    @required double share_price,

  }) : super(
          id: id,
          enterprise_name: enterprise_name,
          photo: photo,
          description: description,
          city: city,
          country: country,
          share_price: share_price,
        );

  factory EnterpriseModel.fromJson(Map<String, dynamic> json) => _$EnterpriseModelFromJson(json);

  Map<String, dynamic> toJson() => _$EnterpriseModelToJson(this);
}
