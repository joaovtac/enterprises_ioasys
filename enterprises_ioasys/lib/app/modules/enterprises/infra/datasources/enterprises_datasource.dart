
import '../../../../modules/enterprises/infra/models/enterprise_model.dart';

abstract class EnterprisesDatasource {
  Future<List<EnterpriseModel>> getListEnterprises(String textSearch);
}
