
import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/errors/errors.dart';
import '../../../../core/errors/messages.dart';
import '../../../../modules/enterprises/domain/entities/enterprise.dart';
import '../../../../modules/enterprises/domain/errors/errors.dart';
import '../../../../modules/enterprises/domain/repositories/enterprises_repository.dart';
import '../../../../modules/enterprises/infra/datasources/enterprises_datasource.dart';
import '../../../../modules/enterprises/infra/models/enterprise_model.dart';

part 'enterprises_repository_impl.g.dart';

@Injectable(singleton: false)
class EnterprisesRepositoryImpl implements EnterprisesRepository {
  final EnterprisesDatasource datasource;

  EnterprisesRepositoryImpl(this.datasource);

  @override
  Future<Either<Failure, List<Enterprise>>> getEnterprises(String searchText) async {
    List<EnterpriseModel> list;

    try {
      list = await datasource.getListEnterprises(searchText);
    } catch (e) {
      return left(ErrorSearch(Messages.FAILED_TO_SEARCH));
    }

    return list == null ? left(DatasourceResultNull(Messages.FAILED_TO_SEARCH)) : right(list);
  }
}
