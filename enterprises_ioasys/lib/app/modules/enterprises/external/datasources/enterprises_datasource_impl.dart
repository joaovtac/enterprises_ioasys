import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/stores/auth_store.dart';
import '../../../../core/utils/utils.dart';
import '../../../../modules/enterprises/infra/datasources/enterprises_datasource.dart';
import '../../../../modules/enterprises/infra/models/enterprise_model.dart';

part 'enterprises_datasource_impl.g.dart';

@Injectable(singleton: false)
class EnterprisesDatasourceImpl implements EnterprisesDatasource {
  final Dio dio;
  final AuthStore authStore;

  EnterprisesDatasourceImpl(this.dio, this.authStore);

  @override
  Future<List<EnterpriseModel>> getListEnterprises(String textSearch) async {

    dio.options.headers.addAll({
      "content-type" : "application/json; charset=utf-8",
      "uid": authStore?.user?.uid,
      "client": authStore?.user?.client,
      "access-token": authStore?.user?.accessToken,
    });

    var result = await this.dio.get("${AppConfig.ENV}/api/${AppConfig.API_VERSION}/enterprises?name=${textSearch.trim().replaceAll(' ', '+')}");
    if (result.statusCode == 200) {
      var jsonList = result.data['enterprises'] as List;
      var listEnterprise = jsonList.map((item) => EnterpriseModel.fromJson(item)).toList();
      return listEnterprise;
    } else {
      throw Exception();
    }
  }
}
