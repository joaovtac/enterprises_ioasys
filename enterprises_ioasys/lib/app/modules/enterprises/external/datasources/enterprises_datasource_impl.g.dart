// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enterprises_datasource_impl.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $EnterprisesDatasourceImpl = BindInject(
  (i) => EnterprisesDatasourceImpl(i<Dio>(), i<AuthStore>()),
  singleton: false,
  lazy: true,
);
