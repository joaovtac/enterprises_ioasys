import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../app_theme.dart';
import '../../../../../../core/utils/utils.dart';
import 'details_enterprise_controller.dart';

class DetailsEnterpriseWidget extends StatelessWidget {
  final _controllerClientRequestsDetails = Modular.get<DetailsEnterpriseController>();

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      final enterprise = _controllerClientRequestsDetails.enterprise;
      return Padding(
        padding: const EdgeInsets.only(top: 40.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: SizedBox(
                    height: 40,
                    width: 40,
                    child: RaisedButton(
                      padding: EdgeInsets.all(0),
                      color: Colors.grey.shade100,
                      child: Center(
                        child: Icon(
                            Icons.arrow_back,
                            size: 20,
                            color: ThemeApp.empresasIoasysColor,
                          ),
                      ),
                      onPressed: () => Modular.to.pop(),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 14, right: 70),
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Center(
                        child: Text(
                          enterprise.enterprise_name,
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Column(
                  children: [
                    Container(
                      color: Colors.grey.shade400,
                      height: 120,
                      width: double.infinity,
                      child: Image.network(
                        "${AppConfig.ENV}${enterprise?.photo}",
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Row(
                          children: [
                            _componetText(
                              title: "Country",
                              value: enterprise?.country,
                            ),
                            _componetText(
                              title: "City",
                              value: enterprise?.city,
                            ),
                            _componetText(
                              title: "Share Price",
                              value: "\$${enterprise?.share_price.toStringAsFixed(2)}",
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 8),
                          child: Text(
                            enterprise.description,
                            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  _componetText({title, value}) {
    return Expanded(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.black,
              ),
              children: <TextSpan>[
                TextSpan(
                  text: title,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(text: "\n$value"),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
