// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'details_enterprise_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $DetailsEnterpriseController = BindInject(
  (i) => DetailsEnterpriseController(i<LoadingDialog>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DetailsEnterpriseController on _DetailsEnterpriseControllerBase, Store {
  final _$enterpriseAtom =
      Atom(name: '_DetailsEnterpriseControllerBase.enterprise');

  @override
  Enterprise get enterprise {
    _$enterpriseAtom.reportRead();
    return super.enterprise;
  }

  @override
  set enterprise(Enterprise value) {
    _$enterpriseAtom.reportWrite(value, super.enterprise, () {
      super.enterprise = value;
    });
  }

  final _$_DetailsEnterpriseControllerBaseActionController =
      ActionController(name: '_DetailsEnterpriseControllerBase');

  @override
  dynamic setEnterprise(Enterprise value) {
    final _$actionInfo = _$_DetailsEnterpriseControllerBaseActionController
        .startAction(name: '_DetailsEnterpriseControllerBase.setEnterprise');
    try {
      return super.setEnterprise(value);
    } finally {
      _$_DetailsEnterpriseControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
enterprise: ${enterprise}
    ''';
  }
}
