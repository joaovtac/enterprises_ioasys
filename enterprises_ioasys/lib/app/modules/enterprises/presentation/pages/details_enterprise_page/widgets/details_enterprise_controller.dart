import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../../../../../modules/enterprises/domain/entities/enterprise.dart';
import '../../../../../../modules/utils/loading_dialog.dart';

part 'details_enterprise_controller.g.dart';

@Injectable()
class DetailsEnterpriseController = _DetailsEnterpriseControllerBase
    with _$DetailsEnterpriseController;

abstract class _DetailsEnterpriseControllerBase with Store {
  final LoadingDialog loading;

  _DetailsEnterpriseControllerBase(this.loading);

  @observable
  Enterprise enterprise;

  @action
  setEnterprise(Enterprise value) => this.enterprise = value;
}
