import 'package:flutter/material.dart';

import 'widgets/details_enterprise_widget.dart';

class DetailsEnterprisePage extends StatefulWidget {
  @override
  _DetailsEnterprisePageState createState() => _DetailsEnterprisePageState();
}

class _DetailsEnterprisePageState extends State<DetailsEnterprisePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DetailsEnterpriseWidget(),
      backgroundColor: Colors.white,
    );
  }
}
