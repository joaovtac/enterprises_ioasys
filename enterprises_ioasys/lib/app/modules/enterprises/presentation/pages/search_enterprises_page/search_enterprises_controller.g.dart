// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_enterprises_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $SearchEnterprisesController = BindInject(
  (i) => SearchEnterprisesController(i<SearchEnterprisesByText>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SearchEnterprisesController on _SearchEnterprisesControllerBase, Store {
  final _$searchTextAtom =
      Atom(name: '_SearchEnterprisesControllerBase.searchText');

  @override
  String get searchText {
    _$searchTextAtom.reportRead();
    return super.searchText;
  }

  @override
  set searchText(String value) {
    _$searchTextAtom.reportWrite(value, super.searchText, () {
      super.searchText = value;
    });
  }

  final _$stateAtom = Atom(name: '_SearchEnterprisesControllerBase.state');

  @override
  SearchState get state {
    _$stateAtom.reportRead();
    return super.state;
  }

  @override
  set state(SearchState value) {
    _$stateAtom.reportWrite(value, super.state, () {
      super.state = value;
    });
  }

  final _$_SearchEnterprisesControllerBaseActionController =
      ActionController(name: '_SearchEnterprisesControllerBase');

  @override
  dynamic setSearchText(String value) {
    final _$actionInfo = _$_SearchEnterprisesControllerBaseActionController
        .startAction(name: '_SearchEnterprisesControllerBase.setSearchText');
    try {
      return super.setSearchText(value);
    } finally {
      _$_SearchEnterprisesControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  dynamic setState(SearchState value) {
    final _$actionInfo = _$_SearchEnterprisesControllerBaseActionController
        .startAction(name: '_SearchEnterprisesControllerBase.setState');
    try {
      return super.setState(value);
    } finally {
      _$_SearchEnterprisesControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
searchText: ${searchText},
state: ${state}
    ''';
  }
}
