import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../core/errors/errors.dart';
import '../../../../../../core/utils/utils.dart';
import '../../../../../../modules/enterprises/presentation/states/search_state.dart';
import '../../../../../../modules/utils/custom_circular_progress_indicator.dart';
import '../search_enterprises_controller.dart';
import 'bar_search_enterprises.dart';
import 'view_list_enterprises.dart';

class ViewStatesResultSearchEnterprises extends StatelessWidget {
  final _controller = Modular.get<SearchEnterprisesController>();
  final String searchText;
  final double heigthKeyboard;

  ViewStatesResultSearchEnterprises({Key key, this.searchText, this.heigthKeyboard}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: searchText.isNotEmpty ? sizedScreen(context).height * 0.075 : sizedScreen(context).height * 0.26,
        ),
        BarSearchEnterprises(
          onChanged: _controller.setSearchText,
        ),
        Expanded(
          child: Observer(
            builder: (_) {
              final state = _controller.state;

              if (state is ErrorState) {
                return _buildError(state.error);
              }

              if (state is StartState) {
                return SizedBox.shrink();
              } else if (state is LoadingState) {
                return Center(
                  child: CustomCircularProgressIndicator(),
                );
              } else if (state is SuccessState) {
                return ViewListEnterprises(
                  list: state.list,
                );
              } else {
                return Container();
              }
            },
          ),
        ),
        SizedBox(
          height: heigthKeyboard,
        )
      ],
    );
  }

  Widget _buildError(Failure error) {
    return Center(
      child: Text(
        error.message,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w300,
          color: Colors.grey.shade400,
        ),
      ),
    );
  }
}
