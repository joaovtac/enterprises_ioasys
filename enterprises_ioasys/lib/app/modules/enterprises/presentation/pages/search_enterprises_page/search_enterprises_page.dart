import 'package:enterprises_ioasys/app/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'search_enterprises_controller.dart';
import 'widgets/background_search_enterprises.dart';
import 'widgets/view_states_result_search_enterprises.dart';

class SearchEnterprisesPage extends StatefulWidget {
  @override
  _SearchEnterprisesPageState createState() => _SearchEnterprisesPageState();
}

class _SearchEnterprisesPageState extends ModularState<SearchEnterprisesPage, SearchEnterprisesController> {
  @override
  Widget build(BuildContext context) {
    final keyboardHeigth = MediaQuery.of(context).viewInsets.bottom;

    return Material(
      color: Colors.white,
      child: Observer(
        builder: (_) {
          final searchText = controller.searchText;
          return Stack(
            children: [
              BackgroundSearchEnterprises(
                searchText: searchText,
              ),
              ViewStatesResultSearchEnterprises(
                heigthKeyboard: keyboardHeigth,
                searchText: searchText,
              ),
              if(searchText.isEmpty)
              Positioned(
                top: sizedScreen(context).height*0.06,
                right: 5,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.white),
                  onPressed: () => Modular.to.pushNamed("/stocks"),
                  child: Text("WebSocket", style: TextStyle(color: Colors.black),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
