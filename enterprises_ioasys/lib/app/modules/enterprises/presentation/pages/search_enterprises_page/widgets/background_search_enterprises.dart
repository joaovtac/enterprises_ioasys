import 'package:flutter/material.dart';

import '../../../../../../core/utils/utils.dart';

class BackgroundSearchEnterprises extends StatelessWidget {
  final String searchText;

  const BackgroundSearchEnterprises({Key key, this.searchText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: searchText.isNotEmpty ? sizedScreen(context).height * 0.12 : sizedScreen(context).height * 0.3,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(
            "assets/app/core/pages/background.png",
          ),
        ),
      ),
      child: Stack(
        children: searchText.isNotEmpty ? [] : _getImagesBackgroundSearch(),
      ),
    );
  }

  List<Widget> _getImagesBackgroundSearch() {
    return [
      getImagePositioned(
        angle: 25.0,
        left: -15.0,
        bottom: -10.0,
        width: 119.0,
        height: 94.0,
      ),
      getImagePositioned(
        angle: 230.0,
        left: 50.0,
        top: -16.0,
        width: 204.74,
        height: 161.73,
      ),
      getImagePositioned(
        angle: 156.0,
        right: 30.0,
        bottom: -4.0,
        width: 119.0,
        height: 94.0,
      ),
      getImagePositioned(
        angle: 160.0,
        right: -25.0,
        top: 20.0,
        width: 119.0,
        height: 94.0,
      ),
    ];
  }

  getImagePositioned({left, right, top, bottom, width, height, angle}) {
    return Positioned(
      left: left,
      bottom: bottom,
      right: right,
      top: top,
      child: RotationTransition(
        turns: AlwaysStoppedAnimation(angle / 360),
        child: Image.asset(
          "assets/app/modules/login/home_logo.png",
          width: width,
          height: height,
          color: Colors.white.withOpacity(0.2),
        ),
      ),
    );
  }
}
