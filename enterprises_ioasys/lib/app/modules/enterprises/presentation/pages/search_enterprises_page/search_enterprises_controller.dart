import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../../domain/usecases/search_enterprises_by_text.dart';
import '../../states/search_state.dart';
import '../details_enterprise_page/widgets/details_enterprise_controller.dart';

part 'search_enterprises_controller.g.dart';

@Injectable()
class SearchEnterprisesController = _SearchEnterprisesControllerBase with _$SearchEnterprisesController;

abstract class _SearchEnterprisesControllerBase with Store {
  final _controllerDetailsEnterprise = Modular.get<DetailsEnterpriseController>();
  final SearchEnterprisesByText searchByText;
  CancelableOperation cancellableOperation;

  _SearchEnterprisesControllerBase(this.searchByText) {
    reaction((_) => searchText, (text) async {
      stateReaction(text, cancellableOperation);
    }, delay: 500);
  }

  Future stateReaction(String text,
      [CancelableOperation cancellableOperation]) async {
    await cancellableOperation?.cancel();
    cancellableOperation =
        CancelableOperation<SearchState>.fromFuture(makeSearch(text));
    if (text.isEmpty) {
      setState(StartState());
      return;
    }
    setState(LoadingState());

    setState(await cancellableOperation.valueOrCancellation(LoadingState()));
  }

  Future<SearchState> makeSearch(String text) async {
    var result = await searchByText(text);
    return result.fold((l) => ErrorState(l), (r) => SuccessState(r));
  }

  @observable
  String searchText = "";

  @action
  setSearchText(String value) => searchText = value;

  @observable
  SearchState state = StartState();

  @action
  setState(SearchState value) => state = value;

  enterDetails(enterprise){
    FocusManager.instance.primaryFocus.unfocus();
    _controllerDetailsEnterprise.setEnterprise(enterprise);
    Modular.to.pushNamed("/enterprises/details");
  }
}
