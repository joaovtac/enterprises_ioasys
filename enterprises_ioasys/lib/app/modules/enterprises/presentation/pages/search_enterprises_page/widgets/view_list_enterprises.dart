import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../core/utils/utils.dart';
import '../../../../../../modules/enterprises/domain/entities/enterprise.dart';
import '../../../../../../modules/enterprises/presentation/pages/search_enterprises_page/search_enterprises_controller.dart';

class ViewListEnterprises extends StatelessWidget {
  final _controller = Modular.get<SearchEnterprisesController>();
  final List<Enterprise> list;

  ViewListEnterprises({Key key, this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16),
            child: Text(
              "${list.length.toString().padLeft(2, "0")} resultados encontrados",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 5.0),
              itemCount: list.length,
              itemBuilder: (_, index) {
                var enterprise = list[index];
                return GestureDetector(
                  onTap: () => _controller.enterDetails(enterprise),
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Container(
                      height: 120,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                          image: NetworkImage(
                            "${AppConfig.ENV}${enterprise?.photo}",
                          )
                        ),
                        color: Colors.grey.shade400.withOpacity(0.25),
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Colors.black38,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(4),
                              bottomRight: Radius.circular(4),
                            ),
                          ),
                          child: SizedBox(
                            width: double.infinity,
                            child: Text(
                              enterprise.enterprise_name,
                              style: TextStyle(
                                color: Colors.white
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
