
import '../../../../core/errors/errors.dart';
import '../../../../modules/enterprises/domain/entities/enterprise.dart';

abstract class SearchState {}

class StartState implements SearchState {
  const StartState();
}

class LoadingState implements SearchState {
  const LoadingState();
}

class ErrorState implements SearchState {
  final Failure error;
  const ErrorState(this.error);
}

class SuccessState implements SearchState {
  final List<Enterprise> list;
  const SuccessState(this.list);
}
