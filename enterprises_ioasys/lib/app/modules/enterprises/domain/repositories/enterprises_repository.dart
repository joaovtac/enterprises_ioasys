
import 'package:dartz/dartz.dart';
import '../../../../core/errors/errors.dart';
import '../../../../modules/enterprises/domain/entities/enterprise.dart';

abstract class EnterprisesRepository {
  Future<Either<Failure, List<Enterprise>>> getEnterprises(String searchText);
}
