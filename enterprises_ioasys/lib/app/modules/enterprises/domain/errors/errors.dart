
import '../../../../core/errors/errors.dart';

class InvalidSearchText extends Failure {
  final String message;
  InvalidSearchText(this.message);
}

class EmptyList extends Failure {
  final String message;
  EmptyList(this.message);

}

class ErrorSearch extends Failure {
  final String message;
  ErrorSearch(this.message);

}

class DatasourceResultNull extends Failure {
  final String message;
  DatasourceResultNull(this.message);

}
