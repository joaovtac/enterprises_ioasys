// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_enterprises_by_text.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $SearchEnterprisesByTextImpl = BindInject(
  (i) => SearchEnterprisesByTextImpl(
      i<EnterprisesRepository>(), i<ConnectivityService>()),
  singleton: false,
  lazy: true,
);
