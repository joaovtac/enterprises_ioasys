import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/connectivity/domain/services/connectivity_service.dart';
import '../../../../core/errors/errors.dart';
import '../../../../core/errors/messages.dart';
import '../../../../modules/enterprises/domain/entities/enterprise.dart';
import '../../../../modules/enterprises/domain/errors/errors.dart';
import '../../../../modules/enterprises/domain/repositories/enterprises_repository.dart';

part 'search_enterprises_by_text.g.dart';

mixin SearchEnterprisesByText {
  Future<Either<Failure, List<Enterprise>>> call(String textSearch);
}

@Injectable(singleton: false)
class SearchEnterprisesByTextImpl implements SearchEnterprisesByText {
  final EnterprisesRepository repository;
  final ConnectivityService service;

  SearchEnterprisesByTextImpl(this.repository, this.service);

  @override
  Future<Either<Failure, List<Enterprise>>> call(String textSearch) async {
    var result = await service.isOnline();

    if (result.isLeft()) {
      return result.map((r) => null);
    }

    var option = optionOf(textSearch);

    return option.fold(() => Left(InvalidSearchText(Messages.INVALID_TEXT_TO_SERACH)), (text) async {
      var result = await repository.getEnterprises(text);
      return result.fold(
        (l) => left(l),
        (r) => r.isEmpty ? left(EmptyList(Messages.IS_EMPTY_LIST)) : right(r),
      );
    });
  }
}
