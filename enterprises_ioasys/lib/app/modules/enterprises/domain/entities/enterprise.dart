class Enterprise {
  final int id;
  final String enterprise_name;
  final String photo;
  final String description;
  final String city;
  final String country;
  final double share_price;

  const Enterprise({this.id, this.enterprise_name, this.photo, this.description, this.city, this.country, this.share_price});
}
