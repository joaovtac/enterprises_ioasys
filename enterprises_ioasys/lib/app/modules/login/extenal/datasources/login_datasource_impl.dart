import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/utils/utils.dart';
import '../../../../modules/login/infra/datasources/login_datasource.dart';
import '../../../../modules/login/infra/models/logged_user_model.dart';

part 'login_datasource_impl.g.dart';

@Injectable(singleton: false)
class LoginDataSourceImpl implements LoginDataSource {
  final Dio dio;

  LoginDataSourceImpl(this.dio);

  @override
  Future<LoggedUserModel> loginEmail({String email, String password}) async {
    final dataCredential = {"email": email, "password": password};

    var result = await dio.post(
      "${AppConfig.ENV}/api/${AppConfig.API_VERSION}/users/auth/sign_in",
      data: dataCredential,
    );

    return LoggedUserModel(
      uid: result.headers.value("uid"),
      client: result.headers.value("client"),
      accessToken: result.headers.value("access-token"),
    );
  }
}
