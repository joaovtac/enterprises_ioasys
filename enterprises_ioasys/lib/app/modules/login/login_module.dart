import 'package:flutter_modular/flutter_modular.dart';

import 'domain/usecases/login_with_email.dart';
import 'extenal/datasources/login_datasource_impl.dart';
import 'infra/repositories/login_repository_impl.dart';
import 'presentation/login_controller.dart';
import 'presentation/login_page.dart';

class LoginModule extends ChildModule {

  @override
  List<Bind> get binds => [
        $LoginRepositoryImpl,
        $LoginDataSourceImpl,
        $LoginController,
        $LoginWithEmailImpl,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter("/", child: (context, args) => LoginPage()),
      ];
}
