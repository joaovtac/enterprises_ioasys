import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../../core/errors/errors.dart';
import '../../../../core/connectivity/domain/services/connectivity_service.dart';
import '../../../../modules/login/domain/entities/logged_user_info.dart';
import '../../../../modules/login/domain/entities/login_credential.dart';
import '../../../../core/errors/messages.dart';
import '../../../../modules/login/domain/errors/errors.dart';
import '../../../../modules/login/domain/repositories/login_repository.dart';

part 'login_with_email.g.dart';

abstract class LoginWithEmail {
  Future<Either<Failure, LoggedUserInfo>> call(LoginCredential credential);
}

@Injectable(singleton: false)
class LoginWithEmailImpl implements LoginWithEmail {
  final LoginRepository repository;
  final ConnectivityService service;

  LoginWithEmailImpl(this.repository, this.service);

  @override
  Future<Either<Failure, LoggedUserInfo>> call(
      LoginCredential credential) async {
    var result = await service.isOnline();

    if (result.isLeft()) {
      return result.map((r) => null);
    }

    if (credential.isEmptyCredentials) {
      return Left(ErrorLoginWithEmail(message: Messages.INVALID_INSERT_CREDENTIALS));
    } else if (!credential.isValidEmail) {
      return Left(ErrorLoginWithEmail(message: Messages.INVALID_FORMAT_EMAIL));
    } else if (!credential.isValidPassword) {
      return Left(ErrorInvalidCredentials(message: Messages.INVALID_CREDENTIALS));
    }

    return await repository.loginEmail(
      email: credential.email,
      password: credential.password,
    );
  }
}
