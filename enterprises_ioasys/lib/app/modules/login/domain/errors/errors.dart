
import '../../../../core/errors/errors.dart';

class ErrorLoginWithEmail extends Failure {
  final String message;
  ErrorLoginWithEmail({this.message});
}

class ErrorInvalidCredentials extends Failure {
  final String message;
  ErrorInvalidCredentials({this.message});
}
