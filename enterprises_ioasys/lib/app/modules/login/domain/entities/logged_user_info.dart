abstract class LoggedUserInfo {
  String get uid;
  String get client;
  String get accessToken;
}
