import 'package:equatable/equatable.dart';

class LoggedUser extends Equatable {
  final String uid;
  final String client;
  final String accessToken;


  const LoggedUser({this.uid, this.client, this.accessToken});

  @override
  List<Object> get props => [uid, client, accessToken];
}
