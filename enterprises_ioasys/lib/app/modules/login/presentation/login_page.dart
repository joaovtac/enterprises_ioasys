import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/utils/utils.dart';
import 'widgets/actions_login.dart';
import 'widgets/background_login.dart';
import 'widgets/form_login.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final keyboardOpen = MediaQuery.of(context).viewInsets.bottom != 0;

    return Scaffold(
      body: SingleChildScrollView(
        reverse: true,
        child: Column(
          children: [
            BackgroundLogin(
              keyboardOpen: keyboardOpen,
            ),
            FormLogin(),
            ActionsLogin(),
          ],
        ),
      ),
    );
  }
}
