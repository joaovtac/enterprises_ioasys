import 'package:flutter/material.dart';

import '../../../../app_theme.dart';
import '../../../../core/errors/errors.dart';
import '../../../../modules/login/domain/errors/errors.dart';

class InputTextFieldLogin extends StatelessWidget {
  final String label;
  final bool obscureText;
  final Function onChanged;
  final Failure failure;
  final TextInputType keyboardType;
  final Function changeObscurePassword;
  final bool isPass;

  const InputTextFieldLogin({
    Key key,
    this.label,
    this.obscureText = false,
    this.onChanged,
    this.failure,
    this.keyboardType,
    this.changeObscurePassword,
    this.isPass = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 4.0, bottom: 4.0),
          child: Text(
            label,
            style: TextStyle(color: ThemeApp.labelColor),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: ThemeApp.greyColor,
            borderRadius: BorderRadius.circular(4),
          ),
          child: TextField(
            obscureText: obscureText,
            keyboardType: keyboardType,
            onChanged: onChanged,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: failure is ErrorInvalidCredentials ? Colors.red : Colors.transparent,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: failure is ErrorInvalidCredentials ? Colors.red : Colors.transparent,
                ),
              ),
              suffixIcon: getSuffixIcon(isPass: isPass),
              isDense: true,
            ),
          ),
        ),
      ],
    );
  }

  getSuffixIcon({isPass = false}) {
    if (failure is ErrorInvalidCredentials) {
      return Icon(
        Icons.cancel,
        color: ThemeApp.redColor,
        size: 20,
      );
    }

    return isPass
        ? GestureDetector(
            onTap: changeObscurePassword,
            child: Icon(
              Icons.remove_red_eye_rounded,
              color: ThemeApp.labelColor,
              size: 22,
            ),
          )
        : SizedBox.shrink();
  }
}
