import 'dart:math';

import '../../../../core/utils/utils.dart';
import 'package:flutter/material.dart';

class BackgroundLogin extends StatelessWidget {
  final bool keyboardOpen;

  const BackgroundLogin({Key key, this.keyboardOpen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: !keyboardOpen ? sizedScreen(context).height * 0.4 : sizedScreen(context).height * 0.2,
        ),
        ClipPath(
          clipper: CurvedBottomClipper(),
          child: Container(
            height: !keyboardOpen ? sizedScreen(context).height * 0.35 : sizedScreen(context).height * 0.175,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  "assets/app/core/pages/background.png",
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/app/modules/login/home_logo.png",
                  height: 40,
                  color: Colors.white,
                ),
                if (!keyboardOpen)
                  SizedBox(
                    height: 16.42,
                  ),
                if (!keyboardOpen)
                  Text(
                    "Seja bem vindo ao empresas!",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class CurvedBottomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final roundingHeight = size.height * 2 / 5;

    final filledRectangle = Rect.fromLTRB(0, 0, size.width, size.height - roundingHeight);
    final roundingRectangle = Rect.fromLTRB(-60, size.height - roundingHeight * 2, size.width + 60, size.height);

    final path = Path();
    path.addRect(filledRectangle);

    path.arcTo(roundingRectangle, pi, -pi, true);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

