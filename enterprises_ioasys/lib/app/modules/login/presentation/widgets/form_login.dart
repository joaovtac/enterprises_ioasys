import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../app_theme.dart';
import 'input_text_field_login.dart';
import '../login_controller.dart';

class FormLogin extends StatelessWidget {
  final _controllerForm = Modular.get<LoginController>();

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InputTextFieldLogin(
                label: "Email",
                onChanged: _controllerForm.setEmail,
                failure: _controllerForm.failure,
                keyboardType: TextInputType.emailAddress,
              ),
              SizedBox(
                height: 16,
              ),
              Column(
                children: [
                  InputTextFieldLogin(
                    label: "Senha",
                    onChanged: _controllerForm.setPassword,
                    failure: _controllerForm.failure,
                    keyboardType: TextInputType.text,
                    obscureText: _controllerForm.obscurePassword,
                    changeObscurePassword: _controllerForm.setObscurePassword,
                    isPass: true,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0, right: 4.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          _controllerForm.failure?.message ?? "",
                          style: TextStyle(
                            color: ThemeApp.redColor,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
