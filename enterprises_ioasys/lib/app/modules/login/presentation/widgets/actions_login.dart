import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../app_theme.dart';
import '../login_controller.dart';

class ActionsLogin extends StatelessWidget {
  final _controllerForm = Modular.get<LoginController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 35.0, right: 35.0, bottom: 30.0),
      child: SizedBox(
        height: 48,
        width: 316,
        child: RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          color: ThemeApp.empresasIoasysColor,
          onPressed: _controllerForm.enterEmail,
          child: Text(
            "ENTRAR",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
