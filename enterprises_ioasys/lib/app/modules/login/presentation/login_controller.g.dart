// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $LoginController = BindInject(
  (i) =>
      LoginController(i<LoginWithEmail>(), i<LoadingDialog>(), i<AuthStore>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginController on _FormLoginControllerBase, Store {
  Computed<LoginCredential> _$credentialComputed;

  @override
  LoginCredential get credential => (_$credentialComputed ??=
          Computed<LoginCredential>(() => super.credential,
              name: '_FormLoginControllerBase.credential'))
      .value;

  final _$emailAtom = Atom(name: '_FormLoginControllerBase.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_FormLoginControllerBase.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$obscurePasswordAtom =
      Atom(name: '_FormLoginControllerBase.obscurePassword');

  @override
  bool get obscurePassword {
    _$obscurePasswordAtom.reportRead();
    return super.obscurePassword;
  }

  @override
  set obscurePassword(bool value) {
    _$obscurePasswordAtom.reportWrite(value, super.obscurePassword, () {
      super.obscurePassword = value;
    });
  }

  final _$failureAtom = Atom(name: '_FormLoginControllerBase.failure');

  @override
  Failure get failure {
    _$failureAtom.reportRead();
    return super.failure;
  }

  @override
  set failure(Failure value) {
    _$failureAtom.reportWrite(value, super.failure, () {
      super.failure = value;
    });
  }

  final _$_FormLoginControllerBaseActionController =
      ActionController(name: '_FormLoginControllerBase');

  @override
  dynamic setEmail(String value) {
    final _$actionInfo = _$_FormLoginControllerBaseActionController.startAction(
        name: '_FormLoginControllerBase.setEmail');
    try {
      return super.setEmail(value);
    } finally {
      _$_FormLoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setPassword(String value) {
    final _$actionInfo = _$_FormLoginControllerBaseActionController.startAction(
        name: '_FormLoginControllerBase.setPassword');
    try {
      return super.setPassword(value);
    } finally {
      _$_FormLoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setObscurePassword() {
    final _$actionInfo = _$_FormLoginControllerBaseActionController.startAction(
        name: '_FormLoginControllerBase.setObscurePassword');
    try {
      return super.setObscurePassword();
    } finally {
      _$_FormLoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setFailure(Failure value) {
    final _$actionInfo = _$_FormLoginControllerBaseActionController.startAction(
        name: '_FormLoginControllerBase.setFailure');
    try {
      return super.setFailure(value);
    } finally {
      _$_FormLoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
email: ${email},
password: ${password},
obscurePassword: ${obscurePassword},
failure: ${failure},
credential: ${credential}
    ''';
  }
}
