import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../../core/errors/errors.dart';
import '../../../core/stores/auth_store.dart';
import '../../utils/loading_dialog.dart';
import '../domain/entities/login_credential.dart';
import '../domain/usecases/login_with_email.dart';

part 'login_controller.g.dart';

@Injectable()
class LoginController = _FormLoginControllerBase with _$LoginController;

abstract class _FormLoginControllerBase with Store {
  final LoginWithEmail loginWithEmailUsecase;
  final LoadingDialog loading;
  final AuthStore authStore;

  _FormLoginControllerBase(
      this.loginWithEmailUsecase, this.loading, this.authStore);

  @observable
  String email = "";

  @action
  setEmail(String value) => this.email = value;

  @observable
  String password = "";

  @action
  setPassword(String value) => this.password = value;

  @observable
  bool obscurePassword = true;

  @action
  setObscurePassword() => this.obscurePassword = !this.obscurePassword;

  @observable
  Failure failure;

  @action
  setFailure(Failure value) => this.failure = value;

  @computed
  LoginCredential get credential =>
      LoginCredential.withEmailAndPassword(email: email, password: password);


  enterEmail() async {
    loading.show();
    await Future.delayed(Duration(milliseconds: 500));
    var result = await loginWithEmailUsecase(credential);
    await loading.hide();
    result.fold((failure) {
      setFailure(failure);
    }, (user) {
      authStore.setUser(user);
      Modular.to.pushNamedAndRemoveUntil("/", (_) => false);
    });
  }
}
