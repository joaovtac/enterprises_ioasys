import '../../../../modules/login/domain/entities/logged_user.dart';
import '../../../../modules/login/domain/entities/logged_user_info.dart';
import 'package:meta/meta.dart';


class LoggedUserModel extends LoggedUser implements LoggedUserInfo {
  LoggedUserModel({@required String uid, @required String client, @required  String accessToken})
      : super( uid: uid, client: client, accessToken: accessToken);

  LoggedUser toLoggedUser() => this;
}
