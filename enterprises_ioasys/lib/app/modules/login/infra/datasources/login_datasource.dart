import 'dart:async';

import '../../../../modules/login/infra/models/logged_user_model.dart';

abstract class LoginDataSource {
  Future<LoggedUserModel> loginEmail({String email, String password});
}
