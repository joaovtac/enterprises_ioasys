import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../../core/errors/errors.dart';
import '../../../../core/errors/messages.dart';
import '../../../../modules/login/domain/entities/logged_user_info.dart';
import '../../../../modules/login/domain/errors/errors.dart';
import '../../../../modules/login/domain/repositories/login_repository.dart';
import '../../../../modules/login/infra/datasources/login_datasource.dart';

part 'login_repository_impl.g.dart';

@Injectable(singleton: false)
class LoginRepositoryImpl implements LoginRepository {
  final LoginDataSource dataSource;

  LoginRepositoryImpl(this.dataSource);

  @override
  Future<Either<Failure, LoggedUserInfo>> loginEmail({String email, String password}) async {
    try {
      var user = await dataSource.loginEmail(email: email, password: password);
      return Right(user);
    } on DioError catch (e) {
      if( e?.response?.data != null &&
          e?.response?.data["errors"][0] == "Invalid login credentials. Please try again."){
        return Left(ErrorInvalidCredentials(message: Messages.INVALID_CREDENTIALS));
      }
      return Left(ErrorLoginWithEmail(message: Messages.FAILED_TO_LOGIN));
    }catch (e) {
      return Left(ErrorLoginWithEmail(message: Messages.FAILED_TO_LOGIN));
    }
  }
}
