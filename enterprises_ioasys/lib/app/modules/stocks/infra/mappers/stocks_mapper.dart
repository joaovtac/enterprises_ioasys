import 'package:enterprises_ioasys/app/modules/stocks/domain/entities/stock.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'stocks_mapper.g.dart';

@JsonSerializable(explicitToJson: true)
class StocksMapper extends Stock {
  StocksMapper({
    @required String name,
    @required String tag,
    @required double price,
  }) : super(
    name: name,
    tag: tag,
    price: price,
  );

  factory StocksMapper.fromJson(Map<String, dynamic> json) => _$StocksMapperFromJson(json);

  Map<String, dynamic> toJson() => _$StocksMapperToJson(this);
}
