// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stocks_mapper.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StocksMapper _$StocksMapperFromJson(Map<String, dynamic> json) {
  return StocksMapper(
    name: json['name'] as String,
    tag: json['tag'] as String,
    price: (json['price'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$StocksMapperToJson(StocksMapper instance) =>
    <String, dynamic>{
      'name': instance.name,
      'tag': instance.tag,
      'price': instance.price,
    };
