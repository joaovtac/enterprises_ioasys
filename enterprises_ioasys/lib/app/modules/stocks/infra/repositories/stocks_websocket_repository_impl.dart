import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../../core/errors/errors.dart';
import '../../../../core/errors/messages.dart';
import '../../../../modules/stocks/domain/entities/stock.dart';
import '../../../../modules/stocks/domain/errors/errors.dart';
import '../../../../modules/stocks/domain/repositories/stocks_websocket_repository.dart';
import '../../../../modules/stocks/infra/datasources/stocks_websocket_datasource.dart';
import '../../../../modules/stocks/infra/mappers/stocks_mapper.dart';

part 'stocks_websocket_repository_impl.g.dart';

@Injectable(singleton: false)
class StocksWebSocketRepositoryImpl implements StocksWebSocketRepository {
  final StocksWebSocketDatasource dataSource;

  StocksWebSocketRepositoryImpl(this.dataSource);

  @override
  Either<Failure, WebSocketChannel> getStreamStocks() {
    try {
        final streamStocks = dataSource.getStockWebSocket();
        return Right(streamStocks);
    } catch (e) {
      return Left(ErrorStocksWebSocket(message: Messages.FAILED_TO_RECOVER_WEBSOCKET_STOCKS));
    }
  }

  @override
  Either<Failure, List<Stock>> buildStocks(String data) {
    try {
        final json = jsonDecode(data);
        final listJson = json['stocks'] as List;
        List<Stock> stocks = listJson.map((item) => StocksMapper.fromJson(item)).toList();

        if(stocks.isEmpty){
          return Left(ErrorStocksWebSocket(message: Messages.NONE_STOCKS_LISTED));
        }

        return Right(stocks);
    } catch (e) {
      return Left(ErrorStocksWebSocket(message: Messages.DATA_INVALID_WEBSOCKET_STOCKS));
    }
  }
}

