
import 'package:enterprises_ioasys/app/core/wrappers/web_socket_wrapper.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'domain/usecases/build_listen_stocks_websocket.dart';
import 'domain/usecases/connect_stocks_websocket.dart';
import 'external/datasources/stocks_websocket_datasource_Impl.dart';
import 'infra/repositories/stocks_websocket_repository_impl.dart';
import 'presentation/stocks_page.dart';
import 'presentation/stocks_page_controller.dart';

class StockModule extends ChildModule {
  static List<Bind> export = [];

  @override
  List<Bind> get binds => [
    $ConnectStocksWebSocketImpl,
    $BuildListenStocksWebSocketImpl,
    $StocksWebSocketRepositoryImpl,
    $StocksWebSocketDatasourceImpl,
    $WebSocketChannelWrapper,
    $StocksPageController,
  ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter("/",
            child: (context, args) => StocksPage(), transition: TransitionType.rightToLeftWithFade, duration: Duration(milliseconds: 500)),
      ];

  static Inject get to => Inject<StockModule>.of();
}
