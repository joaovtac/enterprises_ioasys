// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stocks_websocket_datasource_Impl.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $StocksWebSocketDatasourceImpl = BindInject(
  (i) => StocksWebSocketDatasourceImpl(i<WebSocketChannelWrapper>()),
  singleton: false,
  lazy: true,
);
