import 'package:flutter_modular/flutter_modular.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../../core/utils/utils.dart';
import '../../../../core/wrappers/web_socket_wrapper.dart';
import '../../../../modules/stocks/infra/datasources/stocks_websocket_datasource.dart';

part 'stocks_websocket_datasource_Impl.g.dart';

@Injectable(singleton: false)
class StocksWebSocketDatasourceImpl implements StocksWebSocketDatasource {
  final WebSocketChannelWrapper webSocketChannelWrapper;

  StocksWebSocketDatasourceImpl(this.webSocketChannelWrapper);

  @override
  WebSocketChannel getStockWebSocket() {
    final uri = Uri.parse(AppConfig.URL_WEB_SOCKET);
    final channel = webSocketChannelWrapper.connect(uri);

    return channel;
  }
}
