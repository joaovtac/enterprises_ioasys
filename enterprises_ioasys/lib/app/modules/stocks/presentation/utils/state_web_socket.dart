class StateWebSocketStocks {
  static int get connecting => 1;
  static int get failure => 2;
  static int get connectedSuccess => 3;
}