// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stocks_page_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $StocksPageController = BindInject(
  (i) => StocksPageController(
      i<ConnectStocksWebSocket>(), i<BuildListenStocksWebSocket>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$StocksPageController on _StocksPageControllerBase, Store {
  final _$stateWebSocketAtom =
      Atom(name: '_StocksPageControllerBase.stateWebSocket');

  @override
  int get stateWebSocket {
    _$stateWebSocketAtom.reportRead();
    return super.stateWebSocket;
  }

  @override
  set stateWebSocket(int value) {
    _$stateWebSocketAtom.reportWrite(value, super.stateWebSocket, () {
      super.stateWebSocket = value;
    });
  }

  final _$_StocksPageControllerBaseActionController =
      ActionController(name: '_StocksPageControllerBase');

  @override
  dynamic setStateWebSocket(int value) {
    final _$actionInfo = _$_StocksPageControllerBaseActionController
        .startAction(name: '_StocksPageControllerBase.setStateWebSocket');
    try {
      return super.setStateWebSocket(value);
    } finally {
      _$_StocksPageControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
stateWebSocket: ${stateWebSocket}
    ''';
  }
}
