import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../modules/stocks/presentation/stocks_page_controller.dart';
import 'utils/state_web_socket.dart';
import 'widgets/card_stock.dart';

class StocksPage extends StatefulWidget {
  @override
  _StocksPageState createState() => _StocksPageState();
}

class _StocksPageState extends State<StocksPage> {
  final controller = Modular.get<StocksPageController>();

  @override
  void initState() {
    controller.initStream();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("WebSocket Ações"),
        centerTitle: true,
        backgroundColor: Colors.blueGrey[700],
      ),
      backgroundColor: Colors.grey.shade200,
      body: Observer(
        builder: (_) {
          if (controller.stateWebSocket == StateWebSocketStocks.connecting) {
            return Center(
              child: Text("Conectando à stream..."),
            );
          }

          if (controller.stateWebSocket == StateWebSocketStocks.failure) {
            return Center(
              child: Text(controller.failure.message),
            );
          }

          return Column(
            children: [
              Expanded(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                  itemCount: controller.listStocks.length,
                  itemBuilder: (context, index) {
                    final stock = controller.listStocks[index];
                    return CardStock(stock: stock);
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    controller.webSocketChannel.sink.close();
    super.dispose();
  }
}
