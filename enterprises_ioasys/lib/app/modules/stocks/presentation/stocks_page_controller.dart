
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../core/errors/errors.dart';
import '../../../modules/stocks/domain/entities/stock.dart';
import '../../../modules/stocks/domain/usecases/build_listen_stocks_websocket.dart';
import '../../../modules/stocks/domain/usecases/connect_stocks_websocket.dart';
import 'utils/state_web_socket.dart';

part 'stocks_page_controller.g.dart';

@Injectable()
class StocksPageController = _StocksPageControllerBase with _$StocksPageController;

abstract class _StocksPageControllerBase with Store {
  final ConnectStocksWebSocket connectStocksWebSocketUseCase;
  final BuildListenStocksWebSocket buildListenStocksWebSocketUseCase;

  _StocksPageControllerBase(this.connectStocksWebSocketUseCase, this.buildListenStocksWebSocketUseCase);


  WebSocketChannel webSocketChannel;
  setWebSocketChannel(WebSocketChannel value) => webSocketChannel = value;

  List<Stock> listStocks;
  setListStocks(List<Stock> value) => listStocks = value;

  Failure failure;
  setFailure(Failure value) => failure = value;


  @observable
  int stateWebSocket = StateWebSocketStocks.connecting;

  @action
  setStateWebSocket(int value) => stateWebSocket = value;


  initStream() async {
    final result = await connectStocksWebSocketUseCase();

    result.fold((failure) {
          setFailure(failure);
          setStateWebSocket(StateWebSocketStocks.failure);
        }, (webSocketChannel) {
          setWebSocketChannel(webSocketChannel);
          listenStream();
        }
    );
  }

  listenStream() {
    webSocketChannel.stream.listen((event) async {
      final result = await buildListenStocksWebSocketUseCase(event);
      result.fold((failure) {
        setFailure(failure);
        setStateWebSocket(StateWebSocketStocks.failure);
      }, (listStocks) {
        setListStocks(listStocks);
        setStateWebSocket(StateWebSocketStocks.connectedSuccess);
      });
    });
  }
}
