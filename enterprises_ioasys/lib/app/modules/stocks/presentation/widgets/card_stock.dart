import 'package:flutter/material.dart';

import '../../domain/entities/stock.dart';
import '../../../utils/utils.dart';

class CardStock extends StatelessWidget {
  final Stock stock;

  const CardStock({Key key, this.stock}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          boxShadow: [
            BoxShadow(
              color: Colors.black,
              blurRadius: 4,
              offset: Offset(2, 4),
              spreadRadius: 2,
            )
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              stock.name,
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                stock.tag,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            Text(
              realNotation(stock.price),
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}
