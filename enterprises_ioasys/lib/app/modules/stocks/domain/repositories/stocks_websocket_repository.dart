import 'package:dartz/dartz.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../../core/errors/errors.dart';
import '../../../../modules/stocks/domain/entities/stock.dart';

abstract class StocksWebSocketRepository {
  Either<Failure, WebSocketChannel> getStreamStocks();
  Either<Failure, List<Stock>> buildStocks(String data);
}
