// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'connect_stocks_websocket.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $ConnectStocksWebSocketImpl = BindInject(
  (i) => ConnectStocksWebSocketImpl(
      i<StocksWebSocketRepository>(), i<ConnectivityService>()),
  singleton: false,
  lazy: true,
);
