import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../../core/connectivity/domain/services/connectivity_service.dart';
import '../../../../core/errors/errors.dart';
import '../../../../modules/stocks/domain/repositories/stocks_websocket_repository.dart';

part 'connect_stocks_websocket.g.dart';

abstract class ConnectStocksWebSocket {
  Future<Either<Failure, WebSocketChannel>> call();
}

@Injectable(singleton: false)
class ConnectStocksWebSocketImpl implements ConnectStocksWebSocket {
  final StocksWebSocketRepository repository;
  final ConnectivityService service;

  ConnectStocksWebSocketImpl(this.repository, this.service);

  @override
  Future<Either<Failure, WebSocketChannel>> call() async {
    var result = await service.isOnline();

    if (result.isLeft()) {
      return result.map((r) => null);
    }

    return repository.getStreamStocks();
  }
}
