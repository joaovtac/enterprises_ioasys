// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'build_listen_stocks_websocket.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $BuildListenStocksWebSocketImpl = BindInject(
  (i) => BuildListenStocksWebSocketImpl(
      i<StocksWebSocketRepository>(), i<ConnectivityService>()),
  singleton: false,
  lazy: true,
);
