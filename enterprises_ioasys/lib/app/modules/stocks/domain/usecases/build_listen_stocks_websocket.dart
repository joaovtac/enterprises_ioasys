import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/connectivity/domain/services/connectivity_service.dart';
import '../../../../core/errors/errors.dart';
import '../../../../core/errors/messages.dart';
import '../../../../modules/stocks/domain/entities/stock.dart';
import '../../../../modules/stocks/domain/errors/errors.dart';
import '../../../../modules/stocks/domain/repositories/stocks_websocket_repository.dart';

part 'build_listen_stocks_websocket.g.dart';

abstract class BuildListenStocksWebSocket {
  Future<Either<Failure, List<Stock>>> call(String data);
}

@Injectable(singleton: false)
class BuildListenStocksWebSocketImpl implements BuildListenStocksWebSocket {
  final StocksWebSocketRepository repository;
  final ConnectivityService service;

  BuildListenStocksWebSocketImpl(this.repository, this.service);

  @override
  Future<Either<Failure, List<Stock>>> call(data) async {
    var result = await service.isOnline();

    if (result.isLeft()) {
      return result.map((r) => null);
    }

    if(data == null || data.isEmpty){
      return Left(ErrorStocksWebSocket(message: Messages.DATA_INVALID_WEBSOCKET_STOCKS));
    }

    return repository.buildStocks(data);
  }
}
