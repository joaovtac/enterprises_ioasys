import 'package:equatable/equatable.dart';

class Stock extends Equatable {
  final String name;
  final String tag;
  final double price;

  Stock({this.name, this.tag, this.price});

  @override
  List<Object> get props =>[ name, tag, price];
}