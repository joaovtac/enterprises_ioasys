
import '../../../../core/errors/errors.dart';

class ErrorStocksWebSocket extends Failure {
  final String message;
  ErrorStocksWebSocket({this.message});
}